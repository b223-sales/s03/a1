<?php

class Name{

	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your fullname is $this->firstName $this->middleName $this->lastName.";
	}
}

$name = new Name('Senku', '', 'Ishigami');

class Developer extends Name{

	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}
}

$developer = new Developer('John', 'Finch', 'Smith');

class Engineer extends Name{

	public function printName(){
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
}

$engineer = new Engineer('Harold', 'Myers', 'Reese');