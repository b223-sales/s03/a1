<?php require_once "./code.php" ?>

<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>A03: Classes and Objects</title>
	</head>
	<body>
		<h1>Person</h1>
		<p><?php echo $name->printName(); ?></p>

		<h1>Developer</h1>
		<p><?php echo $developer->printName(); ?></p>

		<h1>Engineer</h1>
		<p><?php echo $engineer->printName(); ?></p>
	</body>
</html>